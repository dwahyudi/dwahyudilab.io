---
title: "Kubernetes Manifest File"
date: 2022-09-04T15:08:54+07:00
tags: ["kubernetes", "deployment", "orchestration"]
draft: true
---

Previously we started a simple deployment using Kubernetes. It works great, but it misses the big advantage of using Kubernetes, that is documenting the infrastructure as a code. We should write a configuration code for Kubernetes to read and deploy. With the help of version control like git, we can monitor, track and revert infrastructure changes from git. With the manifest, we can move our infrastructure wherever we want without being locked by certain vendor. Mostly we manifest our infrastructure as yaml file. This yaml file contains everything about our infrastructure, from pods, deployments, services to app secrets. We can have multiple yaml files so that we can have better reusability and understanding about the whole picture, later on when things getting complex, we might want to leverage [helm](https://github.com/helm/helm "https://github.com/helm/helm"), a package manager for Kubernetes.