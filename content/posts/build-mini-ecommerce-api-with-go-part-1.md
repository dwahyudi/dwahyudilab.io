---
title: "Build Mini E-commerce API With Go Part 1 - The Design"
date: 2022-05-07T12:50:32+07:00
draft: false
tags: ["api-design", "mini-commerce"]
---

In this and next articles, we're going to build a simple e-commerce API with Go and PostgreSQL. We're going to split the project into multiple parts of articles. This first part will mostly talk about API designs for the e-commerce API.

# Overview

A simple e-commerce comprises of these roles:

* Merchant:
    - Manage products data (product name, photo, price, quantity, etc).
* Buyer:
    - View products list.
    - Manage products cart (add to cart, remove from cart, etc).
    - Checkout the cart.

There's administrator role too, it depends on the type of the e-commerce. If it's an exclusive store, the merchant is the administrator, but in larger common marketplaces, merchants only sell their products there, while administrators (owner of the marketplaces) maintain the rules and regulations of the marketplaces.

In marketplaces, it is very common for a single account to be able to sell products (as a merchant) and buy products too (as a buyer).

# API Design

We're going to develop restful APIs for all the roles.

## Registration API Design

For our accounts' registration/sign up (merchants and/or buyers), we're going to use free SMTP services for sending verification email. Even better, we can just use email-mocking service like [mailtrap.io](https://mailtrap.io) for easy testing purpose.

* New account registration, if validated and successful, we send email confirmation with email-confirmation link below. Generate and store email confirmation ID secure random hash.
`POST /accounts/registration/`
* Newly registered account will see email from us, with link something like this. We'll match the confirmation ID hash. If successful, flag account as email-verified. `GET /accounts/registration/email_confirmation?confirmation_hash=abcdefghijklmnoprstu`

## Account API Design

* Login for existing account. `POST /accounts/login/`

From this point, every endpoint below will require user to log in first.

* Logout for logged-in account. `POST /accounts/logout/`
* Forget Password for existing account, account will need to input his/her email, so we can send a link for changing password. Generate a forget password random secure hash tied to the email, in database. `POST /accounts/forget-password/email-verify/`
* Verify forget password hash, can be used to verify if such forget password exists. `GET /accounts/forget-password/verify-hash/`
* Verify account changing password because of forgetting the old password, if successful, invalidate the forget password hash. `POST /accounts/forget-password/change-password/`
* Re-requesting account email confirmation, `POST /accounts/registration/email_confirmation_request`, where user give email address that want to be verified, check if such email address present in our database, check if such email address is not already confirmed.

From this point, every endpoint below will require user to have verified email first.

## Merchant API Design

When successfully registered and email-verified, a account can start to sell products in our marketplace.

### Store API Design

A account can have multiple stores. Let say an account can have at most 3 stores at the same time.

* Create new store. `POST /stores/`
* Update existing store. `PUT /stores/:store_id`
* Delete store, must check if latest order from this store is already completed. `DELETE /stores/:store_id`
* View detail of existing store. `GET /stores/:store_id`
* View list of this account's stores. `GET /stores/`

### Merchant's Products API Design

* Add new product. `POST /stores/:store_id/products/`
* Update existing product. `PUT /stores/:store_id/products/:product_id`
* Delete existing product. `DELETE /stores/:store_id/products/:product_id`
* View detail of existing product. `GET /stores/:store_id/products/:product_id`
* View list of products. `GET /stores/:store_id/products/`
* View orders. This is when other accounts place orders to this account's store. `GET /stores/:store_id/orders`
    - There will be some status of orders: `PENDING`, `MERCHANT_PROCESSED`, `COMPLETED`, `MERCHANT_CANCELLED`, `ACCOUNT_CANCELLED`.
* Update order's status, merchant can update order's status. `PATCH /orders/:order_id`

## Buyer API Design

There are some marketplaces that allow unregistered users to do add to cart activity. Here in our simple e-commerce, we're going to require users to login first before they can add any product their cart. But we're going to allow unregistered users to view products.

* First thing users want to do is mostly searching for product, we're going to utilise ElasticSearch for this one. `GET /marketplace/products/search?query=tomato`
* View store's product. `GET /marketplace/store/:store_id/products/`
* View a product. `GET /marketplace/products/:product_id`
* Add to cart. Cart is temporary place for buyer to create order from. `POST /marketplace/cart/`
* View cart. `GET /marketplace/cart/`
* Create order from cart, while awaiting for payment. For this tutorial, we're going to use a mock payment service that will call our callback api below. `POST /marketplace/orders/`
* Receive response from mock payment service. `POST /marketplace/payment-verified/`
* View list of placed orders. `GET /marketplace/orders/`
* Cancel a placed order, can only be cancelled when merchant hasn't yet processed the order, where does the refund go? To customer's balance. Balance can then be used to buy as well. `PATCH /marketplace/orders/:order_id/cancel`
* View self-profile, `GET /profile/`
* View self current balance amount, `GET /profile/balance`