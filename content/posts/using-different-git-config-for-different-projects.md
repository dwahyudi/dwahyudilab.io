---
title: "Using Different Git Config for Different Projects"
date: 2023-07-16T20:30:42+07:00
draft: false
tags: ["git"]
---

I have a need to use different git config for different projects, here's how I do it.

Let say I have 2 folders, one for my daily work projects and the others for my personal project (like this blog).

Split the configurations into 2 files, let's name it `.gitconfig-work` and `.gitconfig-personal`.

Here's `.gitconfig-work`:

```ini
[user]
	name = Dwi Wahyudi
	email = dwi.wahyudi@work-email.com
```

Here's `.gitconfig-personal`:

```ini
[user]
	name = Dwi Wahyudi
	email = dwi.wahyudi@personal-email.com
```

This is so that when I commit on my work projects git uses my work email, and when I work on my personal project git uses my personal email. And here's `.gitconfig`:

```ini
[includeIf "gitdir:~/Projects/"]
    path = .gitconfig-work
[includeIf "gitdir:~/dwahyudi.gitlab.io/"]
    path = .gitconfig-personal
[includeIf "gitdir:~/personal-projects/"]
    path = .gitconfig-personal
```
