---
title: Hold'em Poker Hands in Ruby (Part 2)
date: 2020-04-19T11:55:00+07:00
draft: false
tags: ["ruby"]
---

After modelling the card and generating 52 of them. It's time to create the deck. Let's shuffle the cards.

# The Deck

```ruby
require_relative 'cards_generator'
require 'securerandom'

class Deck
  def perform
    CardsGenerator
      .new
      .perform
      .shuffle(random: Random.new(SecureRandom.random_number(2**32)))
  end
end
```

My approach for this is the deck will just be stored in simple array, not object. An object of deck will do, but I want to try something different here.

# Deck Manager

With the simple array of shuffled cards (deck), I need another class for managing it.

```ruby
require 'deck'

class DeckManager
  def initialize
    @deck = Deck.new.perform
  end

  def draw(number: 1)
    @deck.shift(number)
  end
end
```

This `DeckManager` class will manage each deck it is generated, including drawing from it.

After this, we can do the following:

```ruby
deck_manager = DeckManager.new
card = deck_manager.draw
# or
hands = deck_manager.draw(5)
```

On the next part, we will try to calculate the values of hands.
