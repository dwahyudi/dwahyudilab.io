---
title: "About Me"
draft: false
date: 2019-04-30T11:26:10+07:00
menu: [navbar]
unlisted: true
---

Overview
========

Hi, I am Dwi Wahyudi,

*   Email: dwi.wahyudi.14\[at\]gmail\[dot\]com
*   Linkedin: [https://www.linkedin.com/in/dwiwahyudi/](https://www.linkedin.com/in/dwiwahyudi/)
*   Gitlab: https://gitlab.com/dwahyudi

A Senior Software Engineer lives in Indonesia.

### Tech Stacks

*   Golang
*   Ruby (Ruby on Rails, Sidekiq, etc)
*   Java (Spring Framework, Hibernate, etc)
*   NodeJS (ExpressJS)
*   Elixir (Phoenix Framework)
*   MySQL / PostgreSQL
*   Redis
*   ElasticSearch
*   RabbitMQ
*   Docker
*   Kubernetes
*   Apache Kafka
*   NATS
*   GRPC

### Current & Previous Work Domains

*   Retailing
*   E-Commerce
*   Marketing Tools Integration
*   Payment Integration
*   HRIS
*   Accounting
*   Inventory System
*   Retail & Distribution
*   Courier & Delivery System
*   Foods and Beverages (Restaurant, Cafe) Management Software

### About This Blog

*   Static Site Generator: [Hugo](https://gohugo.io/)
*   Theme: [Devise](https://themes.gohugo.io/themes/devise/)

If you find mistakes in this blog, please email me: dwi.wahyudi.14\[at\]gmail\[dot\]com